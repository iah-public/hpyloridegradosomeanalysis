"""
A collection of functions used to process super-resolution microscopy detections
Created for A.Tejada project.
"""

import pandas as pd
import numpy as np

import os
import tifffile

import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
#import matplotlib.colors as colors
from matplotlib.colors import ListedColormap
#from matplotlib.collections import PolyCollection
#from mpl_toolkits.mplot3d.art3d import Poly3DCollection

import alphashape
#from shapely.geometry.polygon import Polygon
from shapely.geometry import MultiPoint, Point

from sklearn.cluster import DBSCAN
#from sklearn.preprocessing import StandardScaler

#from skimage.morphology import opening, closing, selem, erosion
#from skimage.filters import gaussian
#from skimage.measure import regionprops, label


def get_data( datadir, data_lbl, load_img_data=False ):
    """Simple data loader; Returns coordinates of 'blinks' detections for 2 channels:
        membrane detections and degradosome detections (xyz, nm).
        Optional output: image as numpy array and calibration nm_per_pixel.
        
        This loader is very specific to the method(output file names) and your 
        data organizatoin. Most likely you will need to imlpement your own.
        
        :param datadir: folder where the data is stored 
        :type datadir:  str
        :param data_lbl: labnel of the dataset
        :type data_lbl: str
        :param load_img_data: Switcher to load the image and calibration instead of coordinates. Changes the size of the output
        :type load_img_data: bool
        :return: tuple of 2: numpy arrays of Nx3 (xyz coordinates) if load_img_data=False; OR tuple of 3:  2 numpy arrays NxM (images) and  1 float (calibration nm per pix) if load_img_data=True
        :rtype: tuple
        """
        
    # data organization table, current dataset.
    df          = pd.read_csv( os.path.join( datadir, 'datasets_superres_example.csv') )
    cur_df       = df[df['data_lbl'] == data_lbl]
    
    # paths to the files containing the detection coordinates. 
    membr_det_path      = os.path.join( datadir, data_lbl, str( cur_df.loc[0,'membrane'] ), cur_df.loc[0,'detections'])
    degr_det_path       = os.path.join( datadir, data_lbl, str( cur_df.loc[0,'degradosome'] ), cur_df.loc[0,'detections'])
  
    membr_coords_xyz     = (pd
                           .read_csv( membr_det_path, delimiter=' ',  names =  ['x (nm)', 'y (nm)', 'z (nm)'], skiprows=1)  
                           .values)
    degr_coords__xyz      = (pd
                           .read_csv( degr_det_path , delimiter=' ',  names =  ['x (nm)', 'y (nm)', 'z (nm)'], skiprows=1)  
                           .values)

    if load_img_data:
        nm_ppix         = cur_df.loc[0, 'nm_pp']
        img_membr_path = os.path.join( datadir, data_lbl, str( cur_df.loc[0,'membrane'] ), cur_df.loc[0,'image'])
        img_degr_path   = os.path.join( datadir, data_lbl, str( cur_df.loc[0,'degradosome'] ), cur_df.loc[0,'image'])
        img_membr       = tifffile.imread( img_membr_path )
        img_degr        = tifffile.imread( img_degr_path  )   
        
        return img_membr, img_degr, nm_ppix
    else:
        return membr_coords_xyz, degr_coords__xyz



def dbscan_2d( xyz, max_dist=100, min_samples=20, Np_range=[200, 3200], R_range=[200, 1000] ):
    """
    Detects clusters using sklearn.clusters.DBSCAN. detected clusters can be filted by number of particles and radius of gyration.

    :param xyz: Numpy array Nx3, [x,y,z] coordinates of the blink detections.
    :type xyz: numpy.ndarray
    :param max_dist: Same as in sklearn.cluster.DBSCAN: maximum distance between points in a cluster
    :type max_dist: float
    :param min_samples: Same as in sklearn.cluster.DBSCAN: minimal number of neighbors for a point to be a core
    :type min_samples: int
    :param Np_range: List of integers; Range [min, max] for N of particles inside a cluster in order for the cluster to be selected.
    :type list:
    :param R_range: List of floats; Range [min, max] for radius of gyration of a cluster in order for the cluster  to be selected.
    :type R_range: list
    :return: Pandas dataframe  [x,y,z, cluster_label, selected]; types: [float, float, float, int, bool]
    :rtype: pandas.DataFrame
    """
    
    # use only [x,y]  for the cluster.
    db = DBSCAN( eps=max_dist, min_samples=min_samples ).fit( xyz[ :, 0:2 ] )
    # core_samples_mask = np.zeros_like( db.labels_, dtype=bool )
    # core_samples_mask[db.core_sample_indices_] = True

    labels = db.labels_
    unique_labels = set(labels)
    
    # Select those clusters that satisfy Np_range and R_range
    # holders for selected labels, radii of gyration
    selected_lbls  = []
    R_of_selected_lbls = []
    
    for ul in unique_labels:
        
        inds = labels==ul
        Np = xyz[ inds ].shape[0]                    
        x = xyz[ inds, 0 ]
        y = xyz[ inds, 1 ]
        cx = x.mean()
        cy = y.mean()
        # gyration radius of this cluster:
        R = np.sqrt( (x-cx)*(x-cx) + (y-cy)*(y-cy) ).mean()

        if Np >= min(Np_range) and Np <= max(Np_range) and R >= min(R_range) and R <= max(R_range):
            selected_lbls.append( ul )  
            R_of_selected_lbls.append( R )
 
    selected_lbls = np.array( selected_lbls )
    R_of_selected_lbls = np.array( R_of_selected_lbls )

    # shift BG from -1 to 0    
    labels = labels + 1 
    selected_lbls = selected_lbls + 1
    
    # shape the output pandas dataframe
    clusters_df = pd.DataFrame( index=range(xyz.shape[0]), columns=['x','y','z','cluster_lbl', 'selected'])
#    clusters_df['x'] = xyz[:,0]
#    clusters_df['y'] = xyz[:,1]
#    clusters_df['z'] = xyz[:,2]
    clusters_df[ ['x', 'y', 'z']] = xyz
    clusters_df['cluster_lbl'] = labels
    clusters_df['selected'] = np.isin( labels, selected_lbls)#.astype(np.uint8)

    return clusters_df


def get_cmap( name, N):
    """Returns cmap with frist color white and transparent.
    >>> get_cmap('jet', 256)
    """
    cmap = plt.cm.get_cmap( name, N)
    cmap = [cmap(i) for i in range(N)]
    cmap[0] = [1,1,1,0]
    cmap = ListedColormap( cmap )
    return cmap




def plot_2d( clusters_df, img=None, nm_ppix=1, only_selected_clusters=True, text_box=True, title_str=None, ax=None ):
    """Simple function to plot the cluster dtaframe. Those that were selected are colored, the rest are semitransparent white."""
    
    labels = clusters_df['cluster_lbl']
    labels_selected = clusters_df[ clusters_df['selected'] ]['cluster_lbl'].unique()

    xyz = clusters_df[ ['x','y','z'] ].values.astype(np.float32)
    
    # Number of clusters in labels, ignoring noise if present.
    # subtract 1 if BG label (-1) is present in the list.
    n_selected_clusters = len(set(labels_selected)) - (1 if -1 in labels_selected else 0)
    

    if ax is None:
        fig, ax = plt.subplots( 1, figsize=(12,12) )
    
    if title_str is None:
        ax.set_title( 'N selected clusters  = %d'%( n_selected_clusters ) )
    else:
        ax.set_title( '%s \n N selected clusters  = %d'%(title_str, n_selected_clusters ) )
    ax.grid(0)
    ax.figure.suptitle('DETECTED CLUSTERS')

    
    # show the image with detections?
    if img is None:
        print('No image; forcing calibration to 1; nm_ppix=1')
    else:
        print('Using provided calibration; nm_ppix=%s'%nm_ppix )
        ax.imshow( img, cmap='Greys_r', alpha=1.0 )
    
    
    if not only_selected_clusters:
        xyz_bg = clusters_df[ ~clusters_df['selected'] ][['x','y','z'] ].values
        ax.plot( xyz_bg[:,0]/nm_ppix, xyz_bg[:,1]/nm_ppix, '.', color=[ 1,1,1, 0.5] )


    # linspace colorset (jet)
    colors = [ plt.cm.jet(each) for each in np.linspace(0, 1, len(labels_selected))]
    
    for k, col in zip(labels_selected, colors):

        # ALL points of the CLASS
        inds = (labels == k)
        Np = inds.sum()
        xy = xyz[ inds ]
        
        x = xy[:, 0 ]
        y = xy[:, 1 ]
        cx = x.mean()
        cy = y.mean()
        R = np.sqrt( (x-cx)*(x-cx) + (y-cy)*(y-cy) ).mean()
        
        
        ax.plot( xy[:, 0]/nm_ppix, xy[:, 1]/nm_ppix, 
                's', color=col, markerfacecolor='None')
        # center:
        ax.plot( xy[:, 0].mean()/nm_ppix, xy[:, 1].mean()/nm_ppix, 
                'o', markerfacecolor=tuple(col),
                     markeredgecolor='k', markersize=8)
        
        if text_box:
            ax.text( xy[:, 0].mean()/nm_ppix, xy[:, 1].mean()/nm_ppix, '%d-%d-%0.2f'%( k, Np, R),
                    ha='center', va='center', color='k', bbox=dict(facecolor='w', edgecolor='k',alpha=0.5))
        else:
            ax.text( xy[:, 0].mean()/nm_ppix, xy[:, 1].mean()/nm_ppix, '%d-%d-%0.2f'%( k, Np, R),
                    ha='center', va='center', color='r' )
            
        
    


def link_2_lbls( clusters_df, l1, l2, R_min ):
    """Simple function to connect 2 clusters.
    Provide the label 1 and label 2. Filter by radius of gyration."""

    labels = clusters_df['cluster_lbl'].values
    labels_selected = clusters_df[ clusters_df['selected']==1 ]['cluster_lbl'].unique()

    xyz = clusters_df[ ['x','y','z'] ].values.astype(np.float32)
    
    # relabel the labels: maximum lbl replaced with minimal
    labels_relbld = labels.copy()
    labels_relbld [ (labels_relbld ==l1) | (labels_relbld ==l2) ] = min(l1,l2)
    
    # filtered large labels: remove the Large label
    large_labels_relbld = labels_selected.copy() #large_labels.copy()
    large_labels_relbld = large_labels_relbld [ large_labels_relbld != max(l1,l2) ]
    
    # refilter:
    large_labels_relbld_filtered  = []
    for ul in large_labels_relbld:
#        if xyz[ labels_relbld==ul ].shape[0] >= new_min_number:
#            large_labels_relbld_filtered.append( ul )   
        
        x = xyz[ labels_relbld==ul, 0 ]
        y = xyz[ labels_relbld==ul, 1 ]
        cx = x.mean()
        cy = y.mean()
        R = np.sqrt( (x-cx)*(x-cx) + (y-cy)*(y-cy) ).mean()
        if R > R_min:
            large_labels_relbld_filtered.append( ul )   
    
    
    large_labels_relbld_filtered = np.array( large_labels_relbld_filtered )
    
    xyz_temp_df = clusters_df.copy()
    xyz_temp_df['cluster_lbl'] = labels_relbld
    xyz_temp_df['selected'] = np.isin( labels_relbld, large_labels_relbld_filtered )#.astype(np.uint8)

    return xyz_temp_df
    

def clusters_to_polygons( clusters_df  ):
    """Conver clusters  (clouds of points) into polygons"""
    
    selected_lbls   = clusters_df[ clusters_df['selected'] ]['cluster_lbl'].unique()
    print( 'number of selected labels: %d'%len(selected_lbls) )
    
    polygons = []
    for ind, lbl in enumerate( selected_lbls ):
        if ind%10==0:
            print('Done %d out of %d'%(ind, len(selected_lbls)) )      
        print( 'Cluster %d'%lbl )
 
        xy_sel = clusters_df[ clusters_df['cluster_lbl']==lbl ][['x','y']].values
        poly = alphashape.alphashape( xy_sel )   
        polygons.append( poly )

    return polygons, selected_lbls
        


def polygons_to_lbl_image( polygons, polygons_lbls, nm_ppix, img_shape ):
    """Convert polygons into labeld image (as if the bacteria were segmented).
    This function is experimental; it is rather slow and needs to be optmized.
    """
    assert len(polygons) == len(polygons_lbls), "Length of the list of polygons should be the same as polygons lbls."
    
    # labeled image (result is similar to segmentarion):
    # 8 bit = maximum 255 labels
    img_lbl         = np.zeros( img_shape, dtype=np.uint16 )    
        
    for ind, (poly,  lbl) in enumerate(zip( polygons, polygons_lbls )):
         
        if ind%10==0:
            print('Done %d out of %d'%(ind, len(polygons_lbls)) )
            
        print( 'Cluster %d'%lbl )
 
        poly_mask = np.zeros( img_shape, dtype=bool)
        
        # use only those coordinates that are in the bbox of the poly
        # p1,p2 = corners of the bbox    
        p1 = np.array( [np.floor(c/nm_ppix)  for c in poly.bounds[0:2] ] ).astype(np.uint32 )
        p2 = np.array( [np.floor(c/nm_ppix)  for c in poly.bounds[2:] ] ).astype(np.uint32 )
        rows = range( p1[1], p2[1]+1 )
        cols = range( p1[0], p2[0]+1 )
        for r in rows:
            for c in cols:        
                if poly.contains( Point(c*nm_ppix,r*nm_ppix) ):
                    poly_mask[r,c]=True
                    # poly_mask = gaussian( poly_mask, 8, preserve_range=True) > 0.5
    
                    img_lbl[ poly_mask ] = lbl #poly_mask ] = l
        
    # simple way to smooth jagged shapes: gaussian smoothing + thresh (per label)
    #img_lbl_smoo = np.zeros_like( img_m, dtype=np.uint8)    
    #for l in selected_lbls:
    #    poly_mask = img_lbl==l
    #    poly_mask_smoo = gaussian( poly_mask, 8, preserve_range=True) > 0.5
    #    img_lbl_smoo[ poly_mask_smoo ] = l

    return img_lbl
