"""
Created on Mon May 14 15:32:26 2018
@author: Dmitry Ershov

This is a class that provides a very simple matplotlib-based GUI to manipulate
parameters of particle (blob) detection.
The main algorithm for blob detection is skimage.feature.blob_log
 
The class is actually a draft work and might be slow. Main reason is that 
I'm not using artists handles and update them; instead, I'm redrawing the full canvas
from scratch, which slows down interactivity. On a strong machine 
(2.4 GHz, 92Gb RAM ) with images up to 1024x1024 I never had any difficulties.
    
USAGE:
    - Control is done via buttons of the GUI.
    - Native control via matplotlib buttons (zoom, navigation) works as well.
    - set parameters (min, max radius, threshold - these are actually inputs
                      for skimage.feature.blob_log, read its documentation)
    - detect
    - switch overlay on/off
    - navigate, zoom using matplotlib buttons
    - output to csv (x, y, r). These outputs are the same as skimage.feature.blob_log

# ISSUES:
    - excel output sheet name cannot be longer than 32 symbols.
    I use the file name as sheet name, so be careful.
    - may be possible that the figure disappears right after it was build. To
    avoid it, create the instance: p = ParticvleDetector(...) and keep the 
    reference in memory; e.g. put it in a list l = [p].
"""


import os, sys
from math import sqrt
import pandas as pd
#import numpy as np
 
#import skimage as sk
import skimage.io
from skimage.feature import blob_log#, blob_dog, blob_doh
# from skimage.morphology import disk, square
# from skimage.filters import median

import matplotlib.pyplot as plt
from matplotlib.widgets import Button, TextBox, Slider


class ParticleDetector(object):
   
    def __init__(self, folder_str, file_list, particle_channel):
        
        self.current_folder     = folder_str
        self.particle_channel   = particle_channel # starts from 0

        self.file_list          = file_list  
        self.current_file_ind   = 0
        self.current_file_name  = ''
        
        # DATA
        self.current_img        = None
        self.p_list             = []

        # DETECTOR SETTINGS
        # size
        self.min_size           = 1.1
        self.max_size           = 2
        # signal strenght
        self.current_thresh     = 0.006

        # FIGURE
        self.fig                    = None
        self.ax                     = None
        self.hand_title             = None
        self.hand_but_prev          = None
        self.hand_but_next          = None  
        self.hand_but_detect        = None   
        self.hand_but_overlay       = None
        self.hand_but_output        = None
        self.hand_txtbox_minSize    = None
        self.hand_txtbox_maxSize    = None
        self.hand_sl_thresh         = None
        self.hand_artist_img0       = None # img1 handles
        self.hand_artist_img1       = None # img2 handles
        self.hand_artist_line       = None # line handles. 1 Line; not a list.
       
        self.interp_method          = 'none' #'nearest'
        self.title_font_size        = 8
        # some inner-working flags
        self.overlay_bool           = True # overlay on off
        self.new_image_bool         = True
        
        self.but_h                  = 0.03 # button relative sizes
        self.but_w                  = 0.03
        self.but_font_size          = 8
        self.sl_val_min             = 0.001
        self.sl_val_max             = 0.05
        self.circle_line_w          = 1
        self.circle_line_col        = 'c'
        self.dots_col               = 'r'
        
        # INITIATE
        self.load_set_cur_img()  
        self.build_fig()
        # self.fig.canvas.mpl_connect('close_event', self.handle_close)

        self.hand_artist_line, = self.ax[0].plot([0],[0],  '.', color=self.dots_col) # empty handle for dots
        self.update_fig() 


    def handle_close(self, event):
        """Custom handling of close button. Connect at initialization!"""
        print('\nHandling closing...')
        try:
            print('Closing handled properly.')
        except:
            print('Closing handled with exception.')

        
    def load_set_cur_img(self):
        """ Updates the file name, Loads image, sets it as current. """
        new_name = self.file_list[ self.current_file_ind ]   
        
        if new_name is not self.current_file_name:
            self.current_file_name = new_name
            current_img = skimage.io.imread( os.path.join( self.current_folder, 
                                                          self.current_file_name ),
                                            as_gray=False, plugin='tifffile' )
            
            print('Current image shape: ')
            print( current_img.shape )
            
            # Take image by outer index :  stack[1,:,:] 
            # normally for stack of grey images (stack of channels)  stack[ [ch1,ch2...], [yy], [xx] ]
            # stack.shape = [3, 512, 512]
            # or
            # Take image by inner index :  stack[:,:,1] 
            # normally for 1 colored image: stack[ [xx], [yy], [r,g,b]]
            # stack.shape = [512, 512, 3]
            
            # Attempt to automatically detect dimensions order.
            min_pix_size = 20 #should be enough
            max_channel_n = 6
            min_channel_n = 2
            if current_img.ndim == 3:
                if (current_img.shape[0] > min_pix_size) and (current_img.shape[1] > min_pix_size) and (current_img.shape[2] >= min_channel_n):
                    # likely that 1,2nd dim is pixels and 3rd - color.
                    self.current_img = current_img[ :, :, self.particle_channel ]  
                elif (current_img.shape[0] < max_channel_n) and (current_img.shape[1] > min_pix_size) and (current_img.shape[2] > min_pix_size):
                    # likely that 1st dim img in a stack and 2,3rd are  pixels amounts.
                    self.current_img = current_img[self.particle_channel, :, :]  
            elif current_img.ndim == 2:
                    # likely that it is the same case as above, but just single image.
                    self.current_img = current_img 
      
            
            self.p_list = [] # if new image: empty particle list
            self.new_image_bool = True
        else:
            print('new image same as old. do nothing.')
        

    def export_data(self):
        if len( self.p_list ) == 0:
            print('List is empty.')
            return
        
        # data
        df     = pd.DataFrame(self.p_list)
        
        file_name = self.current_file_name + '.csv'  
        full_path = os.path.join( self.current_folder, file_name )
        df.to_csv( full_path, sep=';', encoding='utf-8', index=False )
        print( 'Saved an csv file: %s' %file_name ) 


    def overlay_onoff(self):
        # hide circles:
        for p in self.ax[1].patches:
            p.set_visible(self.overlay_bool)
        # hide line (1 line; if more make "for l in lines")
        self.hand_artist_line.set_visible(self.overlay_bool)
        
        
    def f_overlay_onoff(self, event):
        self.overlay_bool = not self.overlay_bool
        self.update_fig()
        
        
    def update_fig(self):
#        self.draw_bg_imgs()
#        self.draw_particles()
        
        if self.new_image_bool:
            # clean first? There are fasrer ways to directly access the data of axes.
            self.ax[0].cla()
            self.ax[1].cla()
            self.hand_artist_line, = self.ax[0].plot([0],[0], '.', color=self.dots_col) # empty handle for dots

            self.ax[0].imshow(self.current_img, cmap = plt.cm.gray, interpolation=self.interp_method) # 
            self.ax[1].imshow(self.current_img, cmap = plt.cm.gray, interpolation=self.interp_method) #   
#            self.ax[0].set_xlim(0, self.current_img.shape[1])
#            self.ax[0].set_ylim(0, self.current_img.shape[0])
#            self.ax[1].set_xlim(0, self.current_img.shape[1])
#            self.ax[1].set_ylim(0, self.current_img.shape[0])
            
            self.hand_title.set_text(self.current_file_name)# 
        else:
            # clear all overlay
            #print(self.hand_artist_line)
#            self.hand_artist_line, = self.ax[0].plot([],[], 'r.') 
            self.hand_artist_line.set_xdata([0])
            self.hand_artist_line.set_ydata([0])

            #self.hand_artist_line, = self.ax[0].plot([], [], 'r.') # empty handle for dots
            self.ax[1].patches.clear()

            # just check particles
            if len(self.p_list) > 0:                
#                self.hand_artist_line = self.ax[0].plot( self.p_list[:, 1], self.p_list[:, 0], 'r.')
                self.hand_artist_line.set_xdata(self.p_list[:, 1])
                self.hand_artist_line.set_ydata(self.p_list[:, 0])
            
                # reset patches:
                for p in self.p_list:
                    y, x, r = p # row, column, radius
                    c = plt.Circle((x, y), r, color=self.circle_line_col, linewidth=self.circle_line_w, fill=False, alpha = 1)
                    #self.ax[1].plot(x,y,'r.')
                    self.ax[1].add_patch(c)
                    
        # update
        self.overlay_onoff()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
        
        # these are not needed if CLA
        #self.ax[0].images[0].norm.vmin/vmax
      
    def detect_particles(self, event):
         self.current_thresh = round(self.hand_sl_thresh.val, 4)
         self.min_size = eval(self.hand_txtbox_minSize.text)
         self.max_size = eval(self.hand_txtbox_maxSize.text)
         
         print(' ')
         print('Detecting params:')
         print('Thresh %s' %self.current_thresh )
         print('Size min: %s' %self.min_size )
         print('Size max: %s' %self.max_size )
         print('Analysing...........')
         self.p_list = blob_log(self.current_img, min_sigma=self.min_size, max_sigma=self.max_size, num_sigma=10, threshold=self.current_thresh ) 
         self.p_list[:, 2] = self.p_list[:, 2] * sqrt(2)
         print('Detected %s particles' %len(self.p_list) )
         
         #self.draw_particles()
         #print('Done')
         self.new_image_bool = False
         self.update_fig()
        
    def f_max_size(self, event):
        self.max_size = eval(event)
        print('Max size  is set to ')
        print(self.max_size)
        
    def f_min_size(self, event):
        self.min_size = eval(event)
        print('Min size  is set to ')
        print(self.min_size)
        
    def f_thresh_box(self, event):
        self.current_thresh = eval(event)
        print('Current thresh is set to ')
        print(self.current_thresh)        
        
    def f_output(self, event):
        self.export_data()
       

    def f_next(self, event):
        self.current_file_ind +=1
        if self.current_file_ind > len(self.file_list) -1:
            self.current_file_ind -= len(self.file_list)
            print('Cycling the list')
        self.load_set_cur_img()
        #self.draw_imgs()
        #self.draw_particles()
        self.update_fig()
        
    def f_prev(self, event):
        self.current_file_ind += -1
        if self.current_file_ind < 0:
            self.current_file_ind += len(self.file_list)
            print('Cycling the list')
            
        self.load_set_cur_img()
        #self.draw_imgs()
        #self.draw_particles()
        self.update_fig()
        
    def f_sl_update(self, val):
        pass
        #print(self.hand_sl_thresh.val)
        #self.fig.canvas.draw_idle()
        
        
    def build_fig(self):
        
        self.fig, self.ax = plt.subplots(1,2, figsize=(16, 8), sharex=True, sharey=True)
        plt.subplots_adjust(left=0.1, bottom=0.05, top=0.8)

        self.hand_artist_img0 = self.ax[0].imshow(self.current_img, cmap = plt.cm.gray, interpolation=self.interp_method)#, vmin=0, vmax=2**16-1)
        self.hand_artist_img1 = self.ax[1].imshow(self.current_img, cmap = plt.cm.gray, interpolation=self.interp_method)#, vmin=0, vmax=2**16-1)
        
        yn = 0
        xn = 0
        ax = plt.axes([0 + xn * self.but_w, 1 - (yn+1)*self.but_h, self.but_w, self.but_h])#0.6, 0.05, 0.1, 0.075])
        self.hand_but_prev = Button(ax, 'Prev')
        self.hand_but_prev.on_clicked(self.f_prev)
        self.hand_but_prev.label.set_fontsize(self.but_font_size)

        xn = 1
        ax = plt.axes([0 + xn * self.but_w, 1 - (yn+1)*self.but_h, self.but_w, self.but_h])#0.6, 0.05, 0.1, 0.075])
        self.hand_but_next = Button(ax, 'Next')
        self.hand_but_next.on_clicked(self.f_next) 
        self.hand_but_next.label.set_fontsize(self.but_font_size)

        xn = 3
        self.hand_title = self.fig.text(0 + xn * self.but_w, 1 - (yn+1)*self.but_h, self.current_file_name , fontsize=self.title_font_size)

        yn += 2 # skip one line = +2
        xn = 0
        axsdet = plt.axes([0 + xn*self.but_w, 1 - (yn+1)*self.but_h, self.but_w, self.but_h])
        self.hand_but_overlay = Button(axsdet, 'Overlay')
        self.hand_but_overlay.on_clicked(self.f_overlay_onoff)
        self.hand_but_overlay.label.set_fontsize(self.but_font_size)
        
        yn += 1
        xn = 1
        slider_width = 5 * self.but_w
        axcolor = 'lightgoldenrodyellow'
        axfreq = plt.axes([0 + xn * self.but_w, 1 - (yn+1)*self.but_h, slider_width, 0.5 *self.but_h] , facecolor=axcolor)
        self.hand_sl_thresh = Slider(axfreq, 'Thresh', self.sl_val_min, self.sl_val_max, valinit=self.current_thresh, valfmt='%1.4f')#, valstep=0.0001)
        self.hand_sl_thresh.on_changed(self.f_sl_update)
        self.hand_sl_thresh.label.set_fontsize(self.but_font_size)

        yn += 1
        xn = 1
        axbox = plt.axes( [0 + xn * self.but_w, 1 - (yn+1)*self.but_h, self.but_w, self.but_h] )
        self.hand_txtbox_minSize = TextBox(axbox, 'Min Size', initial=str(self.min_size) )
        self.hand_txtbox_minSize.on_submit(self.f_min_size)
        self.hand_txtbox_minSize.label.set_fontsize(self.but_font_size)

        yn += 1
        xn = 1
        axbox = plt.axes( [0 + xn * self.but_w, 1 - (yn+1)*self.but_h, self.but_w, self.but_h] )
        self.hand_txtbox_maxSize = TextBox(axbox, 'Max Size', initial=str(self.max_size) )
        self.hand_txtbox_maxSize.on_submit(self.f_max_size)
        self.hand_txtbox_maxSize.label.set_fontsize(self.but_font_size)

        yn += 1
        xn = 0
        axsdet = plt.axes([0 + xn*self.but_w, 1 - (yn+1)*self.but_h, 2*self.but_w, self.but_h])
        self.hand_but_detect = Button(axsdet, 'detect')
        self.hand_but_detect.on_clicked(self.detect_particles)
        self.hand_but_detect.label.set_fontsize(self.but_font_size)

        yn += 2 # skip one line (+2)
        xn = 0
        axout = plt.axes([0 + xn * self.but_w, 1 - (yn+1)*self.but_h, self.but_w, self.but_h])#0.6, 0.05, 0.1, 0.075])
        self.hand_but_output = Button(axout, 'Output', color=(1, 0.75, 0.75))#, color=(0,0.9,1))#, hovercolor='0.95')
        self.hand_but_output.on_clicked(self.f_output)
        self.hand_but_output.label.set_fontsize(self.but_font_size)

        plt.show()
            


     