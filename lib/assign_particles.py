"""A collection of functions used to assign particles to parent cells based 
on the neares point on the edge.
Created for A.Tejada project.

There are internal module parameters that affect many functions; take care in modifying them!
"""

import numpy as np
from math import pi, acos, sqrt
import os

import skimage as sk
import skimage.io as skio
from skimage.measure import label, regionprops
from skimage.morphology import remove_small_holes, opening, skeletonize, medial_axis, skeletonize_3d # skeletonize, thin
from skimage.filters import laplace
from skimage.draw import ellipse#, ellipse_perimeter
from skimage.feature import blob_log#, blob_dog, blob_doh
import scipy

import matplotlib.pyplot as plt
#import matplotlib.patches as patches
import pandas
from scipy import ndimage
from copy import copy
import pandas as pd


# =============================================================================
# MODULE INTERNAL PARAMETERS

parent_dist_cut_off         = 25        # pix; max distance to consider a cell as neigbor
dist_to_edge_cutoff         = 5         # pix
 
# preprocessing of mask?
open_cell_segmentation      = False     # on/off opening
struct_el_size              = 1         # size of structuring element. Radius!!!
fill_holes_in_segmentation  = True     # fill holes in BW image
fill_holes_size             = 100       # fill holes smaller than 100 pix in area

# technical parameters
connectivity_level          = 1         # 1 for 4 and 2 for 8
min_dist                    = 1000      # min_dist to edge:

print_found_parents_message = False

# PLOTTING
annotate_particles          = False     # annotate particles index
annotate_nuclei             = False     # annotate cells index, area other things
show_ellipses               = False
plot_skeleton               = True
zoom_in_on_a_cell           = False
   
folder                      = ''
img_format                  = 'tif'
p_list_format               = 'csv'

# chromatic abberations below
shiftY                      = 0
shiftX                      = 0

small_area_cutoff           = 9
small_skel_cutoff           = 2

normalize_tip_counts_by     = 1
# =============================================================================
    


def assign_particles_to_nuclei( data_dir, cur_data_df ):
        
    all_cells           = pd.DataFrame()
    all_particles       = pd.DataFrame()
    all_heatmap_data    = pd.DataFrame()#(columns=['edx','edy','cx','cy','ch'])

    plot_data_ind       = np.random.choice( cur_data_df.index )
    plotted_data_path   = cur_data_df.loc[ plot_data_ind, 'path' ]
    plotted_once        = False
    print('Chosen to plot first image from : %s' %plotted_data_path )

    for row_ind, row in cur_data_df.iterrows():
        
        cur_data_lbl    = row['data_lbl']
        cur_mut         = row['cond']
        cur_t           = row['t']
        cur_od          = row['od']
        cur_folder      = os.path.join( data_dir, row['path'] )

        cur_data_img_files              = get_cur_img_files_list(  cur_folder  )
        
         # Holders
        particleN_distr                 = []
        all_rel_positions               = []
        area_distr                      = []
        particle_per_area_distr         = []
        angle_distr                     = []
        p_counter                       = 0
        cell_counter                    = 0
        
        data_cells                      = pd.DataFrame( columns=[ 'data_name', 'cell_ind', 'area', 'particle_n', 'p_per_area', 'p_per_area_corr'] )
        data_cells                      = data_cells.astype(dtype= {'data_name':np.str, 'cell_ind':np.uint16, 'area':np.uint16,
                                                                    'particle_n': np.uint16, 'p_per_area': np.float32, 'p_per_area_corr':np.float32})
        
        
        data_particles                  = pd.DataFrame( columns=[ 'data_name', 'cell_ind', 'p_ind', 'p_angle', 'p_rel_pos'] )
        data_particles                  = data_particles.astype(dtype= {'data_name':np.str, 'cell_ind':np.uint16, 'p_ind':np.uint16,
                                                                    'p_angle': np.float32, 'p_rel_pos':np.float32})
        
        
        for cur_files in cur_data_img_files:
            # img_name_prefix, nuclei_img, particle_img, nuclei_mask, particle_list     
            cur_data_name, n_im, p_im, n_mask, p_list = cur_files
            print('\n')
            print(cur_data_name)
            
            # CORRECT PARTICLE POSITIONS
            correct_chromatic_abberation( p_list )
        
            # POREPROCESS SEGMENTATION (fill holes, open image)
            n_mask, n_mask_lbl      = preprocess_nuclei( n_mask )
        
            # CONVERT BINARY SEGMENTS TO CELL OBJECTS WITH PROPERTIES
            cells_rPROP             = convert_lbl_to_cells( n_mask_lbl )
        
            # filter by area:
            cells_rPROP             = filter_cells( cells_rPROP )
            
            # FIND PARTICLE PARENTS
            assign_particles_to_cells( cells_rPROP, p_list )
            
            # ANALYZE PARTICLES
            analyze_particles( cells_rPROP )
        
            # PLOT (only if bools are True)
            if not plotted_once:
                title_str = os.path.join( plotted_data_path, cur_data_name)
                fig, axes = plot_stuff(n_im, n_mask, n_mask_lbl, cells_rPROP, p_im, p_list, title_str)
                plotted_once = True
 
            
            # accumulate stats:
            for cell in cells_rPROP:       
                # print(cell.label)  
                # go over each cell:              
                data_cells = data_cells.append({'data_name' : cur_data_name,
                                               'cell_ind' : cell.label,
                                               'area' : cell.area,
                                               'particle_n' : len( cell.children )}, ignore_index=True)
                # go over each particle in cell:
                for p_ind in range( len( cell.children ) ):                
                    data_particles = data_particles.append({'data_name' : cur_data_name,
                                                            'cell_ind' : cell.label,
                                                            'p_ind' : p_ind,
                                                            'p_angle' : cell.children_angles[ p_ind ],
                                                            'p_rel_pos' : cell.children_rel_pos[ p_ind ] }, ignore_index=True)
    
                het_map_df                  = pd.DataFrame(columns=['mut','t','od','id', 'c_xy',
                                                                    'ed_xy', 'sk_l_pix', 'sk_xy', 
                                                                    'sk_ord_xy_list', 'ch', 'ch_rel_pos',
                                                                    'ch_rel_pos_ind', 'ch_rel_pos_R'])
                het_map_df['mut']           = [cur_mut]
                het_map_df['t']             = [cur_t]
                het_map_df['od']            = [cur_od]
                # het_map_df['id']          = [1]  
                
                het_map_df['c_xy']          = [cell.centroid] #x=cell.centroid[1]
                het_map_df['ed_xy']         = [cell.edge_xy]
    
                het_map_df['sk_l_pix']      = [cell.skel_l]
                het_map_df['sk_xy']         = [cell.skel_xy]                        
                het_map_df['sk_ord_xy_list']= [cell.sk_ord_xy_list]
    
                het_map_df['ch']            = [cell.children]
                het_map_df['ch_rel_pos']    = [cell.children_rel_pos]
                het_map_df['ch_rel_pos_ind']= [cell.children_rel_pos_ind]
                het_map_df['ch_rel_pos_R']  = [cell.children_rel_pos_R]
    
                all_heatmap_data            = all_heatmap_data.append(het_map_df)
                
        # I calculate mean are per OD, since the are may change form condition to condition.    
        data_cells[ 'p_per_area' ]          = data_cells[ 'particle_n' ]  / data_cells[ 'area' ] 
        data_cells[ 'p_per_area_corr' ]     = data_cells[ 'p_per_area' ]  *  data_cells['area' ].mean()
    
        data_cells['od']                    = cur_od
        data_particles['od']                = cur_od    
        data_cells['mut']                    = cur_mut
        data_particles['mut']                = cur_mut
        data_cells['data_lbl']             = cur_data_lbl
        data_particles['data_lbl']         = cur_data_lbl    
        data_cells['t']                     = cur_t
        data_particles['t']                 = cur_t    
        
        
        all_cells                           = all_cells.append( data_cells, ignore_index = True)
        all_particles                       = all_particles.append( data_particles, ignore_index = True)

    # remove rows where there is at least one element with Nan           
    all_cells.dropna()
    all_particles.dropna()

    # reorder columns
    column_order  = ['data_lbl', 'mut', 't', 'od', 'data_name', 'cell_ind', 'area', 'particle_n', 'p_per_area', 'p_per_area_corr']
    all_cells = all_cells[ column_order ]
    column_order  = ['data_lbl', 'mut', 't', 'od', 'data_name', 'cell_ind', 'p_ind', 'p_angle', 'p_rel_pos']
    all_particles = all_particles[ column_order ]

    show_quick_stats(all_cells, all_particles )
    return all_cells, all_particles, all_heatmap_data
    


def show_quick_stats(all_cells, all_particles ):
    
    #  CELL COUNTS 
    print(30*'*' + '   CELL COUNTS  ' + 30*'*' )
    
    for mut_str in all_cells['mut'].unique():
        print(10*'-' + mut_str + 10*'-')
        mut_df      = all_cells[ (all_cells['mut']==mut_str) ]

        for tt in mut_df['t'].unique():
            mut_t_df      = mut_df[ mut_df['t']==tt ]
            cur_ncells  = len( mut_t_df )
            # cur_od      = sub_df['od'].unique()
            print( 'T: %s \t N cells: %s'%(tt, cur_ncells) )
            

    #  PARTICLE COUNTS 
    print(30*'*' + '   PARTICLE COUNTS  ' + 30*'*' )
    
    print( 'T'.center(10,' ') +
           'N foci total'.center(20,' ') + 
           'N foci at 2 poles'.center(20,' ')  + 
           'fraction raw'.center(20,' ') + 
           'fraction corr'.center(20,' ')  )
    
    for mut_str in all_particles['mut'].unique():
        
        print(10*'-' + mut_str + 10*'-')
        mut_df      = all_particles[ (all_particles['mut']==mut_str) ]

        for tt in mut_df['t'].unique():
            mut_t_df      = mut_df[ mut_df['t']==tt ]
            n0          = mut_t_df[ mut_t_df['p_rel_pos']==0 ].shape[0]
            n1          = mut_t_df[ mut_t_df['p_rel_pos']==1 ].shape[0]
            n           = n0+n1        
            cur_nparticles  = len( mut_t_df )
            if cur_nparticles == 0:
                print( ('%s'%tt).center(10,' ') + 
                       ('%s'%cur_nparticles).center(20,' ') + 
                       ('%s'%n).center(20,' ') + 
                       (' 0 particles; cannot normalize by 0. ')
                      )
                continue
            
            # if total count is above 0, continue with normalization
            fr          = n/cur_nparticles 
            fr_corr     = fr/ normalize_tip_counts_by
            cur_od      = mut_t_df['od'].unique()
            # print( 'T: %s \t N foci total: %s \t N foci at 2 poles: %s   \t fraction: %0.2f'%(tt, cur_nparticles, n, fr ) )
            print( ('%s'%tt).center(10,' ') + 
                   ('%s'%cur_nparticles).center(20,' ') + 
                   ('%s'%n).center(20,' ')  + 
                   ('%0.3f'%fr).center(20,' ')  + 
                   ('%0.3f'%fr_corr).center(20,' ') )


def get_cur_img_files_list( folder ):
    print('\n', 100*'=')
    print('Looking in:')
    print(folder)
    
    
    nuclei_files = set()
    particle_files = set()
    mask_files = set()
    xls_files = set()

    for f in os.listdir(folder):
        if f.endswith(p_list_format):
            f = f.split('.')[0] # take before . 
            f = f.split('_')[0] # take before _
            xls_files.add( f ) # collect prefixes
    
    for f in os.listdir(folder):
        if f.endswith(img_format) and (f.rfind('mask') > -1):
            f = f.split('.')[0] # take prefix (remove exts)
            f = f.split('_')[0] # remove _mask
            mask_files.add( f ) # collect prefixes
            
    for f in os.listdir(folder):
        if f.endswith(img_format) and (f.rfind('particles') == -1):
            f = f.split('.')[0] # take prefix (remove exts)
            f = f.split('_')[0] # remove _particles
            particle_files.add( f ) # collect prefixes
    
    for f in os.listdir(folder):
        if f.endswith(img_format) and (f.rfind('nuclei') == -1):
            f = f.split('.')[0] # take prefix (remove exts)
            f = f.split('_')[0] # remove _nuclei
            nuclei_files.add( f ) # collect prefixes

    
    final_set = nuclei_files.intersection(particle_files)
    final_set = final_set.intersection(mask_files)
    final_set = final_set.intersection(xls_files)

    print(final_set)

    return_list = []
    
    for f in sorted(final_set):

        cells_file_name  = f + '_nuclei.' + img_format
        part_file_name  = f + '_particles.' + img_format
        mask_file_name  = f + '_mask.' + img_format
        plist_file_name   = f + '_particles.' + img_format + '.' + p_list_format
        
        nuclei_mask     = skio.imread( os.path.join(folder, mask_file_name), as_gray=True, plugin='tifffile' )    
        cellsImage      = skio.imread( os.path.join(folder, cells_file_name), as_gray=True, plugin='tifffile' )
        particleImage   = skio.imread( os.path.join(folder, part_file_name), as_gray=True, plugin='tifffile' ) 
        
        with open( os.path.join(folder, plist_file_name) ) as xls_file:
#            lines = [l.decode('utf8', 'ignore') for l in xls_file.readlines()]
#            for l in lines:
#                print(l)
            ref_data = pandas.read_csv(xls_file, encoding = 'utf-8', sep=';')#, 'Sheet 1')#, engine = 'openpyxl')
            particle_list = ref_data.values #as_matrix()
            
            return_list.append( [ f, cellsImage, particleImage, nuclei_mask, particle_list] )
    
    print('\nFound %s datasets:'%str(len(return_list)) )
    for f in sorted(final_set):
        print(f)
        
    return return_list


def preprocess_nuclei( nuclei_mask ):
    print('Preprocess nuclei mask ...')
    
    # open image
    if open_cell_segmentation:
        # structuring element
        sel = sk.morphology.selem.diamond(struct_el_size)
        nuclei_mask = opening(nuclei_mask, selem=sel )
        
    # fill holes
    # I DECIDED TO FILL HOLES INDIVIDUALLY FOR EACH CELL
    # OTHERWISE - FILLES HOLES BETWEEN NEIGHBORS
    # 
#    if fill_holes_in_segmentation:
#        nuclei_mask = remove_small_holes( nuclei_mask > 0, min_size=fill_holes_size )  
        
    # LABEL BW IMAGE and GET REGION PROPS
    nuclei_mask_lbl = label( nuclei_mask, connectivity=connectivity_level )

    return nuclei_mask, nuclei_mask_lbl


def convert_lbl_to_cells( cells_LBL ):
    
    print('Extracting cell objects from LBL image ...')

    # empty mask template (0)
    zero_templ = np.zeros(cells_LBL.shape, dtype=np.uint8)
    # 255o is enough, since I'll keeo only one region in this holder (max value will be 1)
    # als, 255 is better for edge detection (something wrong with threshold sensitivity in int16, 64)

    cells_rPROP = regionprops( cells_LBL )

    # add fields; add data to each region.
    # 'children' -    holds children particles
    # 'children_angles'  -  holds angle from long axis to a particle
    for cell_ind, cell in enumerate(cells_rPROP):
        
        # make a new mask  for current nuclei, size of the full BW image
        cell_mask           = zero_templ.copy() # empty mask (0)
        flat_index_array    = np.ravel_multi_index(cell.coords.T, cell_mask.shape) # indices of cell mask flattened
        np.ravel( cell_mask )[ flat_index_array ] = 1
                

        # fill holes
        if fill_holes_in_segmentation:
            cell_mask =  np.array( remove_small_holes( cell_mask, area_threshold=fill_holes_size ) , dtype=np.uint8 )
        # cell_mask = np.array( remove_small_holes( cell_mask > 0, min_size=fill_holes_size), dtype=np.uint8 )    
        # "remove_small_holes" returns bool, but I fix it with converting to array
                
        # skeletonize?
        #skel_mask = skeletonize( cell_mask ) # true/false!!!
        #skel_mask =  medial_axis( cell_mask, return_distance=False)
        skel_mask =  skeletonize_3d( cell_mask )

        # get edges as [x,y]
        edge_mask  = np.array( laplace(cell_mask) > 0, dtype=np.uint8 )   # here might be a bug !!!!!!!! There should be a safer way to get the edge.
        
        
        # process masks
        ed_y, ed_x = np.where( edge_mask > 0) 
        sk_y, sk_x = np.where( skel_mask > 0)
       
        # Find skel ends. Not my code (by Mark Setchell):
        # 1. Black -> 10; White -> 0.
        skel_mask = np.uint8(skel_mask) #0-255
        skel_mask[ np.where( skel_mask == 0) ] = 10 #Black -> 10
        skel_mask[ np.where( skel_mask == 1) ] = 0 # White -> 0
        # 2. convolve with kernel:
        kernel = np.uint8([ [1,  1, 1],
                            [1, 10, 1],
                            [1,  1, 1] ])
    
        skel_mask                   = ndimage.convolve( skel_mask, kernel )
        #skel_mask[ np.where( skel_mask == 70) ] = 0
        skel_ends_y, skel_ends_x    =  np.where( skel_mask == 70 )  # might be empty if spine is 1 pixel
        
        # ADDED FIELDS
        cell.skel_xy            = np.vstack((sk_x, sk_y)).T  # structure [x:, y:]
        cell.skel_ends_xy       = np.vstack((skel_ends_x, skel_ends_y)).T
        
        if cell.skel_ends_xy.shape[0] >= 2:
            cell.skel_l, cell.skel_ln, cell.sk_ord_xy_list  = get_spine_length( cell )
        else:
            cell.skel_l, cell.skel_ln, cell.sk_ord_xy_list = 0, 0, []

        cell.edge_xy            = np.vstack((ed_x, ed_y)).T
        cell.children           = []
        cell.children_rel_pos   = []
        cell.children_rel_pos_ind  = []
        cell.children_rel_pos_R  = []        
        cell.children_angles    = []
        
        set_ellipse_xy(cell)
        
    return cells_rPROP
    
   
def set_ellipse_xy(region):
    ''' elliptical properties of an ROI'''
    N       =  50
    t       = np.linspace(0, pi, N)

    a       = region.major_axis_length/2
    b       = region.minor_axis_length/2
    theta   = -region.orientation # y flipped
    x       = region.centroid[1]
    y       = region.centroid[0]
    
    region.ellipse_x    = x + a * np.cos(2*pi*t) * np.cos(theta) - b * np.sin(2*pi*t) * np.sin(theta)
    region.ellipse_y    = y + a * np.cos(2*pi*t) * np.sin(theta) + b * np.sin(2*pi*t) * np.cos(theta)
    
    # one pole
    angle = 0
    region.pole_x       = x + a * np.cos(angle) * np.cos(theta) - b * np.sin(angle) * np.sin(theta)
    region.pole_y       = y + a * np.cos(angle) * np.sin(theta) + b * np.sin(angle) * np.cos(theta)

    # test pole
    #angle = 0 + pi/10
    #region.pole2_x       = x + a * np.cos(angle) * np.cos(theta) - b * np.sin(angle) * np.sin(theta)
    #region.pole2_y       = y + a * np.cos(angle) * np.sin(theta) + b * np.sin(angle) * np.cos(theta)


def filter_cells( cells_rPROP ):
    print('Filtering out cells by area ...')
    filtered_cells = []
    
    for cell in cells_rPROP:
        
        if ( cell.area > small_area_cutoff ) and ( cell.skel_l > small_skel_cutoff ):
            filtered_cells.append( cell )   
            
    return filtered_cells


def get_spine_length( cell ):
    '''Starts from end of a spine, walks along it and calculates length'''
#    print('Cell ' + str(cell.label) + '; calcuating length...')

#
#    if cell.label == 12:
#        d=2
    
    xy              = cell.skel_xy.copy()
    
    cur_xy_old      = cell.skel_ends_xy[0].copy() # first (any) end; start from [0]
    cur_xy          = cur_xy_old.copy()    
    sk_ord_xy_list  = [cur_xy]

    l               = 0
    ln              = 0
    
    
    while( xy.shape[0] > 0):
#        print('\n')
#        print(xy.shape[0] > 0)
#        print('before ' +str(ln) )
#        print(xy)    
#        print(xy.shape[0])
#        print(cur_xy)
#        
        # dists to curr point
        dists           = np.sum( (xy - cur_xy)**2, axis=1 )
        min_dist_ind    = dists.argmin()
        # delete curr point from spine (found by index of min dist)
        xy              = np.delete(xy, min_dist_ind, axis=0)
        dists           = np.delete(dists, min_dist_ind)
        

        if xy.shape[0] > 0: # check again, because we decrease it again
            # find next current candidate (next closest to the current closest):
            min_dist_ind    = dists.argmin()
            # find next closest pixel:
            cur_xy          = xy[min_dist_ind]
            
            sk_ord_xy_list.append(cur_xy)

            # deleted point couns as +1 in length
            # increase length (should be pithagorian) dl = sqrt(cur_xy - xy[min_dist_ind])
            #l += 1  # just to cound pixels N in the spine
            dl              = sqrt( ((cur_xy - cur_xy_old)**2).sum() )
            l               += dl
            ln              += 1 
            cur_xy_old      = cur_xy
            
        else:
            pass
         
#        print('after ' +str(ln) )
#        print(xy)
#        print(xy.shape[0])  
#        print(cur_xy)
#        print(xy.shape[0] > 0)
        
    return l, ln, sk_ord_xy_list


def assign_particles_to_cells( cells_rPROP, particle_list ):
    
    ''' ASSIGNS PARTICLES TO A NEAREST CELL (found by shortest distance to the edge)
        Inputs: 
        - Cell objects
        - list of particles Nx3. Typical output from blob_log/blob_dog/blob_hog
        
        potential bugs:
            - particle list is Nx3, but can easily be adapted to Nx2
            - edge detection is based on laplace. Can be buggy; careful with dtype of mask (dtype=np.uint8 works well).
            - cells individual masks: holes are filled. It may be good in some cases
              that not filling is better.        
    '''
    print('Looking for parents...')
    # go over each particle in the list:   
    for p_ind, particle in enumerate(particle_list):
        
        # debug block: test particular particles only (by their N position in the list).
        # if p_ind not in [100,106,114,122]: #or p_ind != 100 or p_ind != 122:
        #   continue
        
        # row=y, column=x, radius
        py, px, pr = particle 
        
        # temporary holders forpotential particle's neighbor cells
        potential_parent_ind = []
        potential_parent_dist = []

        # go over each cell now and check if it is within tolerable distance  from this particle
        for cell_ind, cell in enumerate(cells_rPROP):
            cy, cx = cell.centroid
            dist = sqrt((py-cy)**2 + (px-cx)**2)
            if dist < parent_dist_cut_off: 
                # collect potential parents and their distances to Particle
                dists = ((( cell.edge_xy - [px, py] )**2).sum(axis=1)) ** 0.5                
                potential_parent_ind.append(cell_ind)            
                potential_parent_dist.append( dists.min())
        
        # if there are more than zero potential Parents, find the closest:            
        if len(potential_parent_ind) > 0:
            indx = 0
            # find index of minimal distance
            cur_min_dist = min(potential_parent_dist)
            if cur_min_dist <= dist_to_edge_cutoff:
                indx = potential_parent_dist.index(cur_min_dist)
                # append this particle to current nuclei as a child
                cells_rPROP[potential_parent_ind[indx]].children.append(particle)

            # print messages
            if print_found_parents_message:
                print('\n')
                print('Particle: ind, x, y: ' + str(p_ind) + ', ' + str(px) + ', ' + str(py))
                print('Potential Parents: ' + str(potential_parent_ind))
                print('Parents Distances: ' + str(potential_parent_dist))
                print('Chosen Parent with Distsance: ' + str(potential_parent_ind[indx]) + ', ' + str(potential_parent_dist[indx]))
    
    # return the updated cell region props (children)
    # return cells_rPROP
    

def analyze_particles( cells_rPROP ):
    print('Analyzing particles...')
    # calculate angles:
    get_angles( cells_rPROP )
    place_particles_along_spine( cells_rPROP )

    
def place_particles_along_spine( cells_rPROP ):
    for cell_ind, cell in enumerate(cells_rPROP):
        if len(cell.children) > 0 and cell.skel_l > 0:            # if a cell has particles and skeleton is at least 2 pixels
            cell.skel_p_proj_xy = np.zeros(shape=(len(cell.children), 2), dtype='uint16')
    
            for p_ind, p in enumerate(cell.children):
                y, x, r = p
                # find a point on the spine, which is the closest to the particle                
                d_min_ind                   = np.linalg.norm( cell.skel_xy - [x,y] , axis=1 ).argmin()
                #d_min_ind = ((cell.skel_xy - [x,y] ) ** 2).sum(axis = 1).argmin() # no need to sqrt
                point_on_spine              = cell.skel_xy[ d_min_ind ]
                d_min                       = np.linalg.norm( point_on_spine - [x,y] ) 
                    
                cell.skel_p_proj_xy[p_ind]  = point_on_spine
                
                #print(str(cell.label) + ' ' + str(p_ind) + ' '+ str( get_rel_position(cell, point_on_spine ) ) )                   
                cell.children_rel_pos.append( get_rel_position(cell, point_on_spine ) )
                cell.children_rel_pos_ind.append( d_min_ind )
                cell.children_rel_pos_R.append( d_min )

    
def get_rel_position(cell, point_on_spine ):    
       
    l_full      = cell.skel_l 
        
    l_temp      = 0

    xy          = cell.skel_xy.copy()
    cur_xy_old  = cell.skel_ends_xy[0].copy()   
    cur_xy      = cur_xy_old.copy()
    
    while( xy.shape[0] > 0):  
        
#        print('\n')
#        print(xy.shape[0] > 0)
#        print('before ' +str(l))
#        print(xy)    
#        print(xy.shape[0])
#        print(cur_xy)
#        
        # is it in spine?
        if np.all( cur_xy == point_on_spine ):
            break
        
        else:
            # dists to curr point
            dists           = np.sum( (xy - cur_xy)**2, axis=1 )
            min_dist_ind    = dists.argmin()
            # delete curr point from spine (found by index of min dist)
            xy              = np.delete(xy, min_dist_ind, axis=0)
            dists           = np.delete(dists, min_dist_ind)
             
            if xy.shape[0] > 0: # check again, because we decrease it again
                # find next current candidate (next closest to the current closest):
                min_dist_ind    = dists.argmin()
                # find next closest pixel:
                cur_xy          = xy[min_dist_ind]
                
                # deleted point couns as +1 in length
                # increase length (should be pithagorian) dl = sqrt(cur_xy - xy[min_dist_ind])
                #l += 1  # just to cound pixels N in the spine
                l_temp          += sqrt( ((cur_xy - cur_xy_old)**2).sum() )
                cur_xy_old      = cur_xy
                
            else:
                pass
            # finished.
        
#        print('after ' +str(l))
#        print(xy)
#        print(xy.shape[0])  
#        print(cur_xy)
#        print(xy.shape[0] > 0)
    rel_pos = l_temp / l_full
#    if len(rel_pos)==0:
#        d=2
    return rel_pos


def get_angles(cells):
    def length(v):
        return sqrt(v[0]**2 + v[1]**2)
    def dot_product(v1, v2):
        return v1[0] * v2[0] + v1[1] * v2[1]
    def angle(v, w):
        cosx = dot_product(v,w) / ( length(v)*length(w) )
        rad  = acos(cosx) # in radians
        return rad*180/pi # returns degrees
    
    for cell in cells:
        cy, cx      = cell.centroid # order matters in centroid
        pole_x, pole_y      = cell.pole_x, cell.pole_y
        
        v1 = np.array( [ pole_x - cx, pole_y - cy ] )
        
        for p in cell.children:
            part_x = p[1]
            part_y = p[0]
            
            v2 = np.array( [ part_x - cx, part_y - cy ] )
            a = np.round(angle(v1, v2), decimals=2)
            
            cell.children_angles.append( a )


def correct_chromatic_abberation( particle_list ):    
    # p[0,1,2]: [row=y, column=x, radius]
    for p in particle_list:
        p[0] += shiftY
        p[1] += shiftX
    #return particle_list
         
        
def plot_stuff(cellsImage, cells_BW, cells_LBL, cells_rPROP, particleImage, particle_list, title_str):
        
    ''' Plot cells and found children  '''
    fig, axes =  (None, None)

     # change window limits - good for saving,
    if zoom_in_on_a_cell:
        ncells          = len( cells_rPROP )   
        middlecell_n    = round(ncells/2)    
        mc              = cells_rPROP[ middlecell_n ]
        cy,cx           = mc.centroid
        w               = 50
        xlims              = [cx-w/2, cx+w/2]
        ylims              = [cy-w/2, cy+w/2]
        
    else:
        xlims = [0, cells_BW.shape[1]]
        ylims = [0, cells_BW.shape[0]]

    fig, axes = plt.subplots(1, 2, figsize=(20, 10), sharex=True, sharey=True)
    plt.subplots_adjust(left=0.1, bottom=0.1, top=0.8)
    fig.suptitle(title_str)
    for ax in axes:
        ax.set_xlim(xlims)
        ax.set_ylim(ylims)
        ax.invert_yaxis()
        # ax.set_axis_off()

    
    # get colormap corresponding to the ampunt of parent cells:
    cm = plt.get_cmap('jet', cells_LBL.max()+1)
    #rgb_img = sk.color.gray2rgb(sk.img_as_ubyte(nuc)) # blur - nucl
    #rgb_img[:,:,1] = sk.img_as_ubyte(par) # green - particles
    #rgb_img[:,:,2].fill(0)
    
    #  ===========================   axes 1  ===========================
    ax = axes[0]
    ax.set_title('LoG blobs')
    #ax.imshow(rgb_img, vmin=0, vmax=30)#, interpolation=None)#'nearest') 
    ax.imshow(particleImage, interpolation=None)#'nearest') 
    
    for p in particle_list:
        y, x, r = p # row, column, radius
        c = plt.Circle((x, y), r, color='w', linewidth=1, fill=False, alpha = 0.3)
        ax.add_patch(c)
        
    #  ===========================   axes 2  ===========================
    ax = axes[1]    
    ax.imshow(cellsImage, interpolation=None, cmap=plt.cm.gray)
    ax.imshow(cells_LBL, interpolation=None, cmap=cm, alpha=0.3)
    
    
    if annotate_particles:
        for p_ind, p in enumerate(particle_list):
            py, px, pr = p # row, column, radius
            ax.plot(px, py, 'w.')
            ax.text(px, py, str(p_ind))
    
    for c_ind, cell in enumerate(cells_rPROP):
        pass

    if annotate_nuclei:    
        for c_ind, cell in enumerate(cells_rPROP):
            cy, cx = cell.centroid
            ax.text(cx, cy, 'N ' + str(c_ind) + ', A ' + str(cell.area), bbox={'facecolor':cm(c_ind), 'pad':1, 'alpha':0.3})#, color=cm(n_ind))

  
    for c_ind, cell in enumerate(cells_rPROP):
        if show_ellipses:
            ax.plot(cell.ellipse_x, cell.ellipse_y, 'w-')
            ax.plot([cell.centroid[1], cell.pole_x], [cell.centroid[0], cell.pole_y], 'r--')
            ax.plot(cell.pole_x, cell.pole_y, 'rd')
            #ax.plot(cell.pole2_x, cell.pole2_y, 'g.')
            for p_ind, p in enumerate(cell.children):
                #ax.plot(p[1], p[0], 'ro')
                ax.text(p[1], p[0], str(cell.children_angles[p_ind]), color='w')

        
        if len(cell.children) > 0:
            for p_ind, p in enumerate(cell.children):
                cy, cx = cell.centroid
                y, x, r = p # row, column, radius

                # plot connecting lines
#                ax.plot([cx, x], [cy, y], 'k--', linewidth=1)
                ax.plot(cx, cy, 'k.')#, mfc=cm(n_ind), mec='k')
                
                # plot particles
                c = plt.Circle((x, y), r, fc=cm(c_ind), ec='k', linewidth=1, fill=True)   
                ax.add_patch(c)
                #ax.scatter( x, y, s=r, marker='o', c=cm(c_ind), alpha=0.75)
    
    if plot_skeleton:
        for c_ind, cell in enumerate(cells_rPROP):
            cy, cx = cell.centroid
            l           = cell.skel_l
            ln          = cell.skel_ln
            #ax.plot(cx, cy, '.', color='r',  s=1) #linewidth=1 )#, color=cm(n_ind))
            ax.scatter( cell.skel_xy[:,0], cell.skel_xy[:,1], s=30, marker='o', c= cm(c_ind), alpha=0.75)
            ax.scatter( cell.skel_ends_xy[:,0], cell.skel_ends_xy[:,1], s=15, marker='s', c='r', alpha=0.75)
            
            if False:
                if ln > 0:
                    ax.text(cx, cy, 'c: ' + str(c_ind) + '; ln ' + str(ln) + '; l ' + str(round(l,2)), bbox={'facecolor':cm(c_ind), 'pad':1, 'alpha':0.3})#, color=cm(n_ind))
                else:
                    ax.text(cx, cy, 'c: ' + str(c_ind) + '; ln ' + str(ln) + '; l ' + str(round(l,2)), bbox={'facecolor':'w', 'pad':1, 'alpha':1})#, color=cm(n_ind))
            
            # plot info on relative positions
            if ln >= 2 and len(cell.children)>0:
                for p_ind, p in enumerate( cell.children ):
                    y, x, r = p 
                    # connecting lines from particle to PRojections
                    ax.plot([cell.skel_p_proj_xy[p_ind, 0], x], [cell.skel_p_proj_xy[p_ind, 1], y], 'k--', linewidth=1)
                    
                    # mark the projeciton point with text and marker:
                    ax.scatter( cell.skel_p_proj_xy[p_ind, 0], cell.skel_p_proj_xy[p_ind, 1], s=15, marker='d', c='w', alpha=0.75)
                    ax.text(cell.skel_p_proj_xy[p_ind, 0], cell.skel_p_proj_xy[p_ind, 1], str(round(cell.children_rel_pos[p_ind], 2)),
                                                                                    bbox={'facecolor':'w', 'pad':1, 'alpha':0.4})#, color=cm(n_ind))
    return fig, axes

