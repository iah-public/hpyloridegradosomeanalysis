## A collection of tools used for data analysis in the publication
# The RNase J-based RNA degradosome is compartmentalized in the gastric pathogen Helicobacter pylori


### Data organization
- All existing datasets are listed in the file "datasets_exapmle.csv" (in this case it is a shortened list). In this list we provide dataset labels, names of conditions in this dataset, time, OD and path to the experiment. If you dont have time or OD division, you can simply put a fake time or OD in these columns. This file is used as entry point in some of the scripts.
- Each condition has several acquired fields of view (Series001.tif, Series002.tif etc). Notes:
    - Each image was acuired with at least 2 channels - for nuclei and degradosomes (particles). 
    - They are split right away and stored as Series00X_nuclei.tif and Series00X_particles.tif
    - The nuclei are segmented and masks are stored as Series00X_mask.tif
    - Thus, for each FOV there will be 3 files: Series00X_nuclei.tif, Series00X_particles.tif, Series00X_mask.tif.

### Typical workflow
How to use the code is shown in the examples; here we outline some general ideas of what is going on behind the scene:
1. Detect degradosomes (particles) using the interactive ParticleDetector, as shown in "1_test_particle_detector.py".
    - The 3 main parameters are minimum and maximum radii of detected particles, threshold. These are exactly the same parameters used in skimage.feature.blob_log, refer to its documentation for more details.
    - The principle of usage of ParticleDetector is easy to understand from the GUI buttons:
        - to start detection, press "Detect"
        - to switch the overlay "onn/off" press "overlay"
        - to generate output press "output". 
        - native matplotlib navigation toolbar is active; use it to zoom in and pan the images.
    - The output of this step is a csv file, where coordinates and radii of particles are stored. Each FOV has its own csv file; e.g. Series00X_particles.tif.csv

2. Process the masks and particle coordinates together, as shown in "2_test_assign_particles.py"
    - In this script there are several internal steps:
        - particles are assigned to the nearest parent cell (to the nearest point on the cell mask edge)
        - cell skeletons are found
        - particle locations are projected onto skeletons and their relative positions along the skeleton are found. For more details refer to the pulication materials and methods.
    - The script randomly chooses a dataset and visualizes the results. This is done to control the quality of assignment, skeleton finding and location projection. Example of such revisualization can be found in "/images".
    - There are three outputs: data related to cells, data related to particles and data used to plot heatmaps. 
    - Use your favourite plotting library to plot the statistics. 

3. Plotting examples.
    - Example of Parent Assignment ![Example of Parent Assignment](./images/Figure_0.png)
        - colored ROIs: parent cells (here: nuclei)
        - large filled circles: daughter particles
        - dashed black line: connets a daughter particle to the centroid of the parent cell

    - Example of skeleton finding and particle location projection on the skeleton ![Example of Skeleton](./images/Figure_2.png)
        - red dots: poles of a parent cell (here: nucleus)
        - colored dots: skeleton of a parent cell
        - large filled circles: daughter particles
        - dashed black line: connets a daughter particle to a relative skeleton position.
        - numbers: show the coordinate of the relative skeleton position (0/1 - particle is at one of the poles)

    - Scripts and More examples: coming soon.




