"""
This script is for testing ParticleDetector Class.

"""

import os, sys

# To find local version of the library, we assume that internal organization of 
# the project was not modified.
PROJECT_ROOT_DIR    = os.path.abspath("../")
DATA_DIR            = os.path.join( PROJECT_ROOT_DIR, 'data/data_set_od' )

sys.path.append( PROJECT_ROOT_DIR )  
from lib import ParticleDetector


folder              = os.path.join( DATA_DIR, 'set_2/WT/1-20180726 RNJ GFP OD 0.12/' )
file_format         = 'tif' 
contains_str        = 'particles'       
particle_channel    = 1

file_list           = [f for f in os.listdir( folder ) 
                       if f.endswith(file_format)
                       and contains_str in f  ]


ParticleDetector( folder, file_list, particle_channel )
    
