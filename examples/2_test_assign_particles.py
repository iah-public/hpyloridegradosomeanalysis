"""
This script is for testing Assign PArticles Module.

"""
import os, sys
import numpy as np
import pandas as pd

# To find local version of the library, we assume that internal organization of 
# the project was not modified.

PROJECT_ROOT_DIR    = os.path.abspath("../")
DATA_DIR            = os.path.join( PROJECT_ROOT_DIR, 'data/data_set_od' )

sys.path.append( PROJECT_ROOT_DIR )  
from lib import assign_particles as al

# ---------------------------------------------------------------------
# IMPORTANT!!!
# Inside the module assign_particles there is a set of parameteres.
# Modifying them in the way below changes them for the whole module.

# Chromatic abberations: correct green G to map it to blue B
# B[x,y] = G[x,y] + shift_g_to_b[x,y]; shift obtained experimentally.
# Set to 0 if you have not performed the alignment.
al.shiftY                       = -1.57142857
al.shiftX                       = 1.71428571

# parameter to normalize counts of pole-associated degradosomes
# (they are biased to higher numbers because of additional degree of freedom)
tip_r                           = 2.5 # D/2; D = typicall nucle width in pix
al.normalize_tip_counts_by      = np.pi * tip_r # Tip perimeter is pi*R: half of the circle perimeter. 
# ---------------------------------------------------------------------



# get data subset

datasets_df              = pd.read_csv( DATA_DIR + '/datasets_od.csv'  )
selected_data_df         = datasets_df[
                                        (datasets_df['data_lbl']=='set_2')
                                       # & (datasets_df['cond']=='WT')
                                      ]

all_cells, all_particles, all_heatmap_data = al.assign_particles_to_nuclei( DATA_DIR,  selected_data_df )







